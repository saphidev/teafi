package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/mail"
	"net/smtp"
	"os"
	"strings"

	"github.com/scorredoira/email"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
)

/*
	CREATED BY SAFI EDDINE
	email / saphidev@gmail.com
	github / apotox
	gitlab / saphidev
	twitter / saphidev
*/

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
func InsertPermission(d *drive.Service, fileId string, permType string, role string) error {
	p := &drive.Permission{Type: permType, Role: role}

	_, err := d.Permissions.Create(fileId, p).Do()
	if err != nil {
		fmt.Printf("An error occurred: %v\n", err)
		return err
	}
	return nil
}

func send_mail(fileId string) {

	var TEAFI_EMAILSTO, OK_TEAFI_EMAILSTO = os.LookupEnv("TEAFI_EMAILSTO")
	if !OK_TEAFI_EMAILSTO {
		log.Println("TEAFI_EMAILSTO env variable missed!")
		return
	}
	var TEAFI_SMTP_USER, OK_TEAFI_SMTP_USER = os.LookupEnv("TEAFI_SMTP_USER")
	if !OK_TEAFI_SMTP_USER {
		log.Println("TEAFI_SMTP_USER env variable missed!")
		return
	}
	var TEAFI_SMTP_PASS, OK_TEAFI_SMTP_PASS = os.LookupEnv("TEAFI_SMTP_PASS")
	if !OK_TEAFI_SMTP_PASS {
		log.Println("TEAFI_SMTP_PASS env variable missed!")
		return
	}

	to := []string{}
	if strings.Contains(TEAFI_EMAILSTO, ",") {
		to = strings.Split(TEAFI_EMAILSTO, ",")
	} else {
		to = []string{TEAFI_EMAILSTO}
	}

	var CI_COMMIT_TITLE, ok_title = os.LookupEnv("CI_COMMIT_TITLE")
	if !ok_title {
		log.Println("CI_COMMIT_TITLE env variable missed!")
		CI_COMMIT_TITLE = "NO_TITLE"
	}

	var GITLAB_USER_NAME, ok_user = os.LookupEnv("GITLAB_USER_NAME")
	if !ok_user {
		log.Println("GITLAB_USER_NAME env variable missed!")
		GITLAB_USER_NAME = "NO_USER"
	}

	m := email.NewHTMLMessage("Release Codes Generator Serice"+CI_COMMIT_TITLE,
		fmt.Sprintf("a new version has been released 👌 <br/>"+
			"<b>%s</b> by <font color='gray'>%s</font> <a style='padding: 4px;border: 1px solid #ccc;margin: 4px;border-radius: 5px;display: inline-block;text-decoration: none;' href='https://drive.google.com/open?id=%s'>Download</a>",
			CI_COMMIT_TITLE,
			GITLAB_USER_NAME,
			fileId))

	m.From = mail.Address{Name: "CI service", Address: TEAFI_SMTP_USER}
	m.To = to

	m.AddHeader("X-CUSTOMER-id", "GITLAB CI BY SAFI EDDINE BOUHETALA")

	// send it
	auth := smtp.PlainAuth("", TEAFI_SMTP_USER, TEAFI_SMTP_PASS, "smtp.gmail.com")
	if err := email.Send("smtp.gmail.com:587", auth, m); err != nil {
		log.Fatal(err)
	}

	log.Println("ok")
}

func main() {

	//read path of file which will be uploaded
	var TEAFI_FILEPATH, OK_TEAFI_FILEPATH = os.LookupEnv("TEAFI_FILEPATH")
	if !OK_TEAFI_FILEPATH {
		log.Fatalf("TEAFI_FILEPATH env variable missed!")
		return
	}

	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	input, err := os.Open(TEAFI_FILEPATH)
	if err != nil {
		fmt.Printf("An error occurred: %v\n", err)
		return
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, drive.DriveFileScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	srv, err := drive.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Drive client: %v", err)
	}

	f := &drive.File{Description: "version compressed", Name: TEAFI_FILEPATH, MimeType: "application/octet-stream"}

	var TEAFI_DRIVE_FOLDERID, OK_TEAFI_DRIVE_FOLDERID = os.LookupEnv("TEAFI_DRIVE_FOLDERID")
	if !OK_TEAFI_DRIVE_FOLDERID {
		log.Fatalf("TEAFI_DRIVE_FOLDERID env variable missed")
	} else {
		f.Parents = []string{TEAFI_DRIVE_FOLDERID}
	}

	//inputInfo, err := input.Stat()
	res, err := srv.Files.Create(f).Media(input, googleapi.ContentType("application/octet-stream")).Do()
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	InsertPermission(srv, res.Id, "anyone", "reader")

	send_mail(res.Id)

}
